# EclecticSE SLK-581 Code Library

The SLK-581 is a fourteen character statistical linkage code used by
Australian Government departments.

## SLK-581 Algorithm

The code is composed of the ordered concatenation of the following
four data elements related to a person:

1. 2nd, 3rd and 5th letters of last name (family name).

2. 2nd and 3rd letters of first (given) name.

3. The date of birth as a character string in the form DDMMYYYY.

4. The sex of the client as:

    * Code "1" for Male
    * Code "2" for Female
    * Code "3" for Other, "intersex" or "indeterminate"
    * Code "9" for Unknown, not stated or inadequately described

### Additional Notes

* Characters not counted are hyphens, apostrophes, blank spaces, or
  any other character which is not a letter of the alphabet, that may
  appear in a name.

* Where the name is not long enough to supply all the required
  letters, fill the remaining places with a 2 to indicate that a
  letter does not exist. This will occur if the first name is less
  than 3 characters and if the last name is less than 5 characters.

* Where a name or part of a name is missing, substitute a 9 to
  indicate that the letter is unknown.

* Use uppercase letters only for SLK.

## Licence

LLGPL

