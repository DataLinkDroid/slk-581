(in-package #:eclecticse.slk-581)

;;; A data record is of the following form, containing at least the
;;; four data items, first name, surname, sex/gender and date of birth:

;; ((:first-name "John")
;;  (:surname "Smithsonian")
;;  (:gender "Male")
;;  (:dob "1963-03-17"))


(defun field-val (item)
  "Return the value of ITEM, which is a (key value) pair."
  (second item))


(defun trim (string)
  "Remove spaces from start and end of STRING."
  (string-trim '(#\Space) string))


(defun cleanse-item (item)
  "Trim ITEM and convert to upper case if a string."
  (if (stringp item)
      (string-upcase (trim item))
      item))


(defun get-item (which data-rec)
  "Retrieve a (key value) pair from the DATA-REC. WHICH is one of
-- :first-name :surname :gender :dob :slk"
  (cleanse-item (field-val (assoc which data-rec))))


(defun cleanse-name (name)
  "Remove non-alpha characters from NAME."
  (remove-if-not #'alpha-char-p name))


(defun get-surname (data-rec)
  "Retrieve the cleansed surname from DATA-REC."
  (cleanse-name (get-item :surname data-rec)))


(defun get-first-name (data-rec)
  "Retrieve the cleansed first name from DATA-REC."
  (cleanse-name (get-item :first-name data-rec)))


(defun get-surname-letters (data-rec)
  "Retrieve the three letters of the surname which form part of the
SLK code."
  (let* ((surname (get-surname data-rec))
         (surname-length (length surname))
         (surname-missing (= 0 surname-length)))
    (cond ((or surname-missing (= 1 surname-length))
           "999")
          ((< surname-length 3)
           (concatenate 'string (list (char surname 1)
                                      #\2
                                      #\2)))
          ((< surname-length 5)
           (concatenate 'string (list (char surname 1)
                                      (char surname 2)
                                      #\2)))
          (t
           (concatenate 'string (list (char surname 1)
                                      (char surname 2)
                                      (char surname 4)))))))


(defun get-first-name-letters (data-rec)
  "Retrieve the two letters of the first name which form part of the SLK code."
  (let* ((first-name (get-first-name data-rec))
         (first-name-length (length first-name))
         (first-name-missing (= 0 first-name-length)))
    (cond ((or first-name-missing (= 1 first-name-length))
           "99")
          ((< first-name-length 3)
           (concatenate 'string (list (char first-name 1) #\2)))
          (t
           (concatenate 'string (list (char first-name 1) (char first-name 2)))))))


(defun iso-date->ddmmyyyy (iso-date)
  "Convert an ISO 8601 date with hyphens into DDMMYYYY."
  (if (>= (length iso-date) 10)
      (concatenate 'string
                   (subseq iso-date 8 10)
                   (subseq iso-date 5 7)
                   (subseq iso-date 0 4))
      "99999999"))


(defun get-dob (data-rec)
  "Retrieve the date of birth in ddmmyyyy format, from DATA-REC."
  (let ((dob (get-item :dob data-rec)))
    (iso-date->ddmmyyyy dob)))


(defun get-sex-code (data-rec)
  "Retrieve the SLK sex/gender code for DATA-REC."
  (let* ((sex (get-item :gender data-rec))
         (sex-sym (if (stringp sex)
                      (cond ((string-equal "male" sex)
                             :male)
                            ((string-equal "female" sex)
                             :female)
                            ((or (string-equal "other" sex)
                                 (string-equal "intersex" sex)
                                 (string-equal "indeterminate" sex))
                             :other)
                            (t
                             :unknown))
                      sex)))
    (cond ((eq sex-sym :male)
           "1")
          ((eq sex-sym :female)
           "2")
          ((eq sex-sym :other)
           "3")
          ((eq sex-sym :unknown)
           "9")
          (t            ; No other possibilities. Should not get here.
           "9"))))

