(in-package #:eclecticse.slk-581)


(defparameter *test-data*
  '(((:first-name "Jane") (:surname "Citizen")
     (:gender "Female") (:dob "1963-05-27")
     (:slk "ITZAN270519632"))
    ((:first-name "Joseph") (:surname "Bloggs")
     (:gender "Male") (:dob "1959-12-31")
     (:slk "LOGOS311219591"))
    ((:first-name "Jane") (:surname "Luca")
     (:gender "Female") (:dob "1963-05-27")
     (:slk "UC2AN270519632"))
    ((:first-name "Jo") (:surname "O'Donnell")
     (:gender "Female") (:dob "1963-05-27")
     (:slk "DONO2270519632"))
    ((:first-name "J") (:surname "Bloggs")
     (:gender "Female") (:dob "1963-05-27")
     (:slk "LOG99270519632"))
    ((:first-name "J") (:surname "Blog")
     (:gender "Female") (:dob "1963-05-27")
     (:slk "LO299270519632"))
    ((:first-name "J") (:surname "O")
     (:gender "Male") (:dob "1959-12-31")
     (:slk "99999311219591"))
    ((:first-name "J") (:surname "Blog")
     (:gender "Not stated") (:dob "1967-06-20")
     (:slk "LO299200619679"))
    ((:first-name "Joseph") (:surname "Bloggs")
     (:gender "Intersex") (:dob "1959-12-31")
     (:slk "LOGOS311219593")))
  "Test data with known correct SLK-581 values.")


(defparameter *slk-scanner* (cl-ppcre:create-scanner
                             (concatenate 'string
                                          "([9]{3}|[A-Z]([2]{2}|[A-Z][A-Z,2]))([9]{2}|[A-Z][A-Z,2])"
                                          "(((((0[1-9]|[1-2][0-9]))|(3[01]))((0[13578])|(1[02])))|"
                                          "((((0[1-9]|[1-2][0-9]))|(30))((0[469])|(11)))|((0[1-9]|"
                                          "[1-2][0-9])02))(19|2[0-9])[0-9]{2}[1 2 3 9]"))
  "Regular expression to validate structure of an SLK-581 code.")
