;;;; eclecticse.slk-581.asd

(asdf:defsystem #:eclecticse.slk-581
  :description "Generate Australian Government SLK-581 codes."
  :author "David K. Trudgett <David.Trudgett@eclecticse.com.au"
  :license  "LLGPL"
  :version "1.0.0"
  :serial t
  :depends-on (#:cl-ppcre)
  :components ((:file "package")
               (:file "data-rec")
               (:file "specials")
               (:file "slk-581")))
