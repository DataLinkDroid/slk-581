;;;; package.lisp

(defpackage #:eclecticse.slk-581
  (:use #:cl)
  (:export #:slk-581
           #:data-rec->slk-581
           #:test-slk-581
           #:regex-check-slk-581))
