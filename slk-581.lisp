;;;; slk-581.lisp

(in-package #:eclecticse.slk-581)



;; Example data record.
;;
;; ((:first-name "John")
;;  (:surname "Smithsonian")
;;  (:gender "Male")
;;  (:dob "1963-03-17"))

(defun slk-581 (given-name family-name date-of-birth sex)
  "Return the SLK-581 code for the given data. DATE-OF-BIRTH is a
  string in YYYY-MM-DD (ISO 8601) format. Sex is one of the following
  values, which match Australian government
  classification: :MALE, :FEMALE, :OTHER, :UNKNOWN, or the strings,
  'Male', 'Female', 'Other', 'Unknown', 'Intersex', or
  'Indeterminate'. Unknown is the same a 'not stated' or 'inadequately
  described'."
  (let ((data-rec (list (list :first-name given-name)
                        (list :surname family-name)
                        (list :gender sex)
                        (list :dob date-of-birth))))
    (data-rec->slk-581 data-rec)))


(defun data-rec->slk-581 (data-rec)
  "Generate the SLK-581 code for DATA-REC."
  (concatenate 'string
               (get-surname-letters data-rec)
               (get-first-name-letters data-rec)
               (get-dob data-rec)
               (get-sex-code data-rec)))


(defun test-slk-581 ()
  "Run tests against all test data records and report the results."
  (let ((results (mapcar (lambda (data-rec)
                           (if (string= (data-rec->slk-581 data-rec) (get-item :slk data-rec))
                               :pass
                               :fail))
                         *test-data*)))
    (if (every (lambda (result)
                 (eq result :pass))
               results)
        "All tests pass."
        results)))


(defun regex-check-slk-581 (slk-581)
  (equal 0 (cl-ppcre:scan *slk-scanner* slk-581)))



